#ifndef SYNRAD48_H
#define	SYNRAD48_H

#ifdef	__cplusplus
extern "C" {
#endif

/* define the xtal frequency for the delay functions */
#define _XTAL_FREQ 4000000

#include <xc.h>

#define LONG_STARTUP_DELAY_MS           5000
#define SHORT_STARTUP_DELAY_MS          250
#define LED_DELAY_MS                    200

/* please see documentation/ADC value computations.xlsx */
#define IPOWLO                  0x1F5 /* 18VDC */
#define POW_TOO_LOW             0x1A2 /* 15VDC */
#define POW_TOO_HIGH            0x3EB /* 36VDC */

#define TEMP_WARN_ON_THRESHOLD  0x18A /* 50degC */
#define TEMP_WARN_OFF_THRESHOLD 0x15C /* 45degC */
#define TEMP_FATAL_THRESHOLD    0x1E6 /* 60degC */

#define MAX_PWM_DIFF            0x90  /* Value adjusted for new 10-bit ADC */
    
#define EXT_FAULT_CHECK         0

enum monitor_status {
        board_power_down = 0,
        board_temp_fatal,
        board_ext_fault_check,
        board_fet_fail,
        board_power_fatal,
        board_power_insufficient,
        led_off,
};

struct ledseq_struct {
        enum monitor_status stat;
        unsigned char led1 : 1;
        unsigned char led2 : 2;
        unsigned char led3 : 3;
};

struct series48_control_board_struct {
        int (*read_power_analog)(struct series48_control_board_struct *brd);
        int (*key_reset_stat)(struct series48_control_board_struct *brd);
        int (*key_stat)(struct series48_control_board_struct *brd);
        void (*board_power_en)(struct series48_control_board_struct *brd, const int on);
        int (*long_timer_sel_check)(struct series48_control_board_struct *brd);
        int (*read_temp_analog)(struct series48_control_board_struct *brd);
        void (*set_temp_warning)(struct series48_control_board_struct *brd, const int on);
        int (*read_ext_fault)(struct series48_control_board_struct *brd);
        int (*read_cmdin)(struct series48_control_board_struct *brd);
        int (*read_pwmout)(struct series48_control_board_struct *brd);
        void (*reset_board)(struct series48_control_board_struct *brd);
        void (*shutdown_board)(struct series48_control_board_struct *brd);
        void (*set_led)(struct series48_control_board_struct *brd,
                        const struct ledseq_struct *pattern);
        
        /* turn on/off the pwm circuitry */
        void (*pwm_switch)(struct series48_control_board_struct *brd,
                           const int enable);
        void *private;
};

extern void register_synrad48_control_board(struct series48_control_board_struct *bd);

#ifdef	__cplusplus
}
#endif

#endif	/* SYNRAD48_H */

