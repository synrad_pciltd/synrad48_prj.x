
// PIC16F18344 Configuration Bit Settings
// 'C' source line config statements
// CONFIG1
#pragma config FEXTOSC = XT     // FEXTOSC External Oscillator mode Selection bits (XT (crystal oscillator) from 100 kHz to 4 MHz)
#pragma config RSTOSC = EXT1X   // Power-up default value for COSC bits (EXTOSC operating per FEXTOSC bits)
#pragma config CLKOUTEN = OFF   // Clock Out Enable bit (CLKOUT function is disabled; I/O or oscillator function on OSC2)
#pragma config CSWEN = OFF       // Clock Switch Enable bit (Writing to NOSC and NDIV is allowed)
#pragma config FCMEN = OFF       // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is enabled)

// CONFIG2
#pragma config MCLRE = ON       // Master Clear Enable bit (MCLR/VPP pin function is MCLR; Weak pull-up enabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config WDTE = OFF       // Watchdog Timer Enable bits (WDT disabled; SWDTEN is ignored)
#pragma config LPBOREN = OFF    // Low-power BOR enable bit (ULPBOR disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable bits (Brown-out Reset disabled)
#pragma config BORV = HIGH      // Brown-out Reset Voltage selection bit (Brown-out voltage (Vbor) set to 2.7V)
#pragma config PPS1WAY = ON    // PPSLOCK bit One-Way Set Enable bit (The PPSLOCK bit can be set and cleared repeatedly (subject to the unlock sequence))
#pragma config STVREN = ON     // Stack Overflow/Underflow Reset Enable bit (Stack Overflow or Underflow will not cause a Reset)
#pragma config DEBUG = OFF      // Debugger enable bit (Background debugger disabled)

// CONFIG3
#pragma config WRT = OFF        // User NVM self-write protection bits (0000h to 0FFFh write protected, no addresses may be modified)
#pragma config LVP = ON        // Low Voltage Programming Enable bit (High Voltage on MCLR/VPP must be used for programming.)

// CONFIG4
#pragma config CP = OFF         // User NVM Program Memory Code Protection bit (User NVM code protection disabled)
#pragma config CPD = OFF        // Data NVM Memory Code Protection bit (Data NVM code protection disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include "synrad48.h"

static const struct ledseq_struct led_patterns[] = {
        { .stat = board_ext_fault_check,    .led1 = 0, .led2 = 1, .led3 = 0, },
        { .stat = board_power_insufficient, .led1 = 1, .led2 = 1, .led3 = 0, },
        { .stat = board_temp_fatal,         .led1 = 1, .led2 = 0, .led3 = 1, },
        { .stat = board_power_fatal,        .led1 = 0, .led2 = 1, .led3 = 1, },
        { .stat = board_fet_fail,           .led1 = 1, .led2 = 1, .led3 = 1, },
        { .stat = led_off,                  .led1 = 0, .led2 = 0, .led3 = 0, },
        {0},
};

static int synrad48_read_power_analog(struct series48_control_board_struct *brd)
{
        return brd->read_power_analog(brd);
}

static int synrad48_read_temp_analog(struct series48_control_board_struct *brd)
{
        return brd->read_temp_analog(brd);
}

static void synrad48_board_power_switch(struct series48_control_board_struct *brd, const int on)
{
        brd->board_power_en(brd, on);
        return;
}

#define synrad48_turn_on_board(brd) \
        do { synrad48_board_power_switch(brd, 1); } while(0)
#define synrad48_turn_off_board(brd) \
        do { synrad48_board_power_switch(brd, 0); } while(0)

static void 
synrad48_board_startup_pwr_protection(struct series48_control_board_struct *brd)
{
        int pwr_lvl;
        do {
                pwr_lvl = synrad48_read_power_analog(brd);
        } while(pwr_lvl < IPOWLO);
        return;
}

static void synrad48_set_pwm(struct series48_control_board_struct *brd, const int on)
{
        brd->pwm_switch(brd, on);
        return;
}
#define synrad48_turn_on_pwm(brd)  do { synrad48_set_pwm(brd, 1); } while(0)
#define synrad48_turn_off_pwm(brd) do { synrad48_set_pwm(brd, 0); } while(0)

static int synrad48_key_reset_enabled(struct series48_control_board_struct *brd)
{
        return (brd->key_reset_stat(brd) == 1);
}

static int synrad48_key_is_on(struct series48_control_board_struct *brd)
{
        return (brd->key_stat(brd) == 1);
}

/* if key_reset is enabled, key needs to be cycled off-on */
static void synrad48_key_ctrl(struct series48_control_board_struct *brd)
{
        int key_reset_enabled;

        key_reset_enabled = synrad48_key_reset_enabled(brd);
        if (key_reset_enabled)
                while(synrad48_key_is_on(brd));
        while(!synrad48_key_is_on(brd));
        return;
}

static void synrad48_startup_delay(struct series48_control_board_struct *brd)
{
        int long_timer_sel;

        long_timer_sel = brd->long_timer_sel_check(brd);
        if (long_timer_sel)
                __delay_ms(LONG_STARTUP_DELAY_MS);
        else
                __delay_ms(SHORT_STARTUP_DELAY_MS);
        return;
}

#if (EXT_FAULT_CHECK == 1)
static int synrad48_read_ext_fault(struct series48_control_board_struct *brd)
{
        return brd->read_ext_fault(brd);
}
#endif

static int synrad48_pwm_fet_fail (struct series48_control_board_struct *brd)
{
		int cmd, pwm;
		
        cmd = brd->read_cmdin(brd);
		pwm = brd->read_pwmout(brd);
		
		return pwm - cmd;
}

static void synrad48_reset_board(struct series48_control_board_struct *brd)
{
        brd->reset_board(brd);
        return;
}

static void synrad48_set_temp_warning(struct series48_control_board_struct *brd,
                                      const int on)
{
        brd->set_temp_warning(brd, on);
        return;
}

static const struct ledseq_struct *
synrad48_get_led_pattern(const enum monitor_status stat)
{
        const struct ledseq_struct *pattern = led_patterns;
        for (; pattern->stat != 0; pattern++) {
                if (pattern->stat == stat)
                        return pattern;
        }
        return pattern;
}

static enum monitor_status
synrad48_shutdown_board(struct series48_control_board_struct *brd,
                        enum monitor_status stat)
{
        const struct ledseq_struct *pattern0; /* on */
        const struct ledseq_struct *pattern1; /* off */

        synrad48_turn_off_board(brd);
        synrad48_turn_off_pwm(brd);
        brd->shutdown_board(brd);
        pattern0 = synrad48_get_led_pattern(stat);
        pattern1 = synrad48_get_led_pattern(led_off);
        do {
                brd->set_led(brd, pattern0);
                __delay_ms(LED_DELAY_MS);
                brd->set_led(brd, pattern1);
                __delay_ms(LED_DELAY_MS);
        } while (synrad48_key_is_on(brd));
        return stat;
}

static enum monitor_status
synrad48_monitor(struct series48_control_board_struct *brd)
{
        int power;
        int key_is_on;
        int temp;
#if (EXT_FAULT_CHECK == 1)
        int ext_fault;
#endif
        int pwm_diff;
        
        for (;;) {
                power = synrad48_read_power_analog(brd);
                if (power < (POW_TOO_LOW-1))
                        return board_power_insufficient;
                
                if (power > (POW_TOO_HIGH-1)) {
                        return board_power_fatal;
                }

                key_is_on = synrad48_key_is_on(brd);
                if (!key_is_on)
                     break;

                temp = synrad48_read_temp_analog(brd);
                if (temp > (TEMP_WARN_ON_THRESHOLD-1)) {
                        synrad48_set_temp_warning(brd, 1);
                        if (temp > (TEMP_FATAL_THRESHOLD-1))
                                return board_temp_fatal;
                } else if (temp < (TEMP_WARN_OFF_THRESHOLD-1)) {
                    synrad48_set_temp_warning(brd, 0);
                }
#if (EXT_FAULT_CHECK == 1)
                ext_fault = synrad48_read_ext_fault(brd);
                if (ext_fault)
                        return board_ext_fault_check;
#endif
                synrad48_turn_on_pwm(brd);

                pwm_diff = synrad48_pwm_fet_fail(brd);
                if (pwm_diff >= MAX_PWM_DIFF)
                        return board_fet_fail;
        }
        
        return board_power_down;
}

void register_synrad48_control_board(struct series48_control_board_struct *brd)
{
        enum monitor_status stat;

        synrad48_turn_off_pwm(brd);
        synrad48_board_startup_pwr_protection(brd);
        for (;;) {
                synrad48_reset_board(brd);
                synrad48_key_ctrl(brd);
                synrad48_turn_on_board(brd);
                synrad48_startup_delay(brd);
                stat = synrad48_monitor(brd);
                switch(stat) {
                case board_power_insufficient:
                case board_power_fatal:
                case board_temp_fatal:
                case board_ext_fault_check:
                case board_fet_fail:
                        synrad48_shutdown_board(brd, stat);
                        break;
                case board_power_down:
                default:
                        synrad48_turn_off_board(brd);
                        synrad48_turn_off_pwm(brd);
                        break;
                }
        }
        return;
}
