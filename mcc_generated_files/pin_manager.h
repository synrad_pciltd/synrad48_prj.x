/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.3
        Device            :  PIC16F18344
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.20 and above
        MPLAB 	          :  MPLAB X 5.40	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set shutdown aliases
#define shutdown_TRIS                 TRISAbits.TRISA2
#define shutdown_LAT                  LATAbits.LATA2
#define shutdown_PORT                 PORTAbits.RA2
#define shutdown_WPU                  WPUAbits.WPUA2
#define shutdown_OD                   ODCONAbits.ODCA2
#define shutdown_ANS                  ANSELAbits.ANSA2
#define shutdown_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define shutdown_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define shutdown_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define shutdown_GetValue()           PORTAbits.RA2
#define shutdown_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define shutdown_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define shutdown_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define shutdown_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define shutdown_SetPushPull()        do { ODCONAbits.ODCA2 = 0; } while(0)
#define shutdown_SetOpenDrain()       do { ODCONAbits.ODCA2 = 1; } while(0)
#define shutdown_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define shutdown_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set keyren_led1 aliases
#define keyren_led1_TRIS                 TRISBbits.TRISB4
#define keyren_led1_LAT                  LATBbits.LATB4
#define keyren_led1_PORT                 PORTBbits.RB4
#define keyren_led1_WPU                  WPUBbits.WPUB4
#define keyren_led1_OD                   ODCONBbits.ODCB4
#define keyren_led1_ANS                  ANSELBbits.ANSB4
#define keyren_led1_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define keyren_led1_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define keyren_led1_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define keyren_led1_GetValue()           PORTBbits.RB4
#define keyren_led1_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define keyren_led1_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define keyren_led1_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define keyren_led1_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define keyren_led1_SetPushPull()        do { ODCONBbits.ODCB4 = 0; } while(0)
#define keyren_led1_SetOpenDrain()       do { ODCONBbits.ODCB4 = 1; } while(0)
#define keyren_led1_SetAnalogMode()      do { ANSELBbits.ANSB4 = 1; } while(0)
#define keyren_led1_SetDigitalMode()     do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set lsectm_led2 aliases
#define lsectm_led2_TRIS                 TRISBbits.TRISB5
#define lsectm_led2_LAT                  LATBbits.LATB5
#define lsectm_led2_PORT                 PORTBbits.RB5
#define lsectm_led2_WPU                  WPUBbits.WPUB5
#define lsectm_led2_OD                   ODCONBbits.ODCB5
#define lsectm_led2_ANS                  ANSELBbits.ANSB5
#define lsectm_led2_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define lsectm_led2_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define lsectm_led2_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define lsectm_led2_GetValue()           PORTBbits.RB5
#define lsectm_led2_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define lsectm_led2_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define lsectm_led2_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define lsectm_led2_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)
#define lsectm_led2_SetPushPull()        do { ODCONBbits.ODCB5 = 0; } while(0)
#define lsectm_led2_SetOpenDrain()       do { ODCONBbits.ODCB5 = 1; } while(0)
#define lsectm_led2_SetAnalogMode()      do { ANSELBbits.ANSB5 = 1; } while(0)
#define lsectm_led2_SetDigitalMode()     do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set msgctl_led3 aliases
#define msgctl_led3_TRIS                 TRISBbits.TRISB6
#define msgctl_led3_LAT                  LATBbits.LATB6
#define msgctl_led3_PORT                 PORTBbits.RB6
#define msgctl_led3_WPU                  WPUBbits.WPUB6
#define msgctl_led3_OD                   ODCONBbits.ODCB6
#define msgctl_led3_ANS                  ANSELBbits.ANSB6
#define msgctl_led3_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define msgctl_led3_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define msgctl_led3_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define msgctl_led3_GetValue()           PORTBbits.RB6
#define msgctl_led3_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define msgctl_led3_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define msgctl_led3_SetPullup()          do { WPUBbits.WPUB6 = 1; } while(0)
#define msgctl_led3_ResetPullup()        do { WPUBbits.WPUB6 = 0; } while(0)
#define msgctl_led3_SetPushPull()        do { ODCONBbits.ODCB6 = 0; } while(0)
#define msgctl_led3_SetOpenDrain()       do { ODCONBbits.ODCB6 = 1; } while(0)
#define msgctl_led3_SetAnalogMode()      do { ANSELBbits.ANSB6 = 1; } while(0)
#define msgctl_led3_SetDigitalMode()     do { ANSELBbits.ANSB6 = 0; } while(0)

// get/set ib_link aliases
#define ib_link_TRIS                 TRISBbits.TRISB7
#define ib_link_LAT                  LATBbits.LATB7
#define ib_link_PORT                 PORTBbits.RB7
#define ib_link_WPU                  WPUBbits.WPUB7
#define ib_link_OD                   ODCONBbits.ODCB7
#define ib_link_ANS                  ANSELBbits.ANSB7
#define ib_link_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define ib_link_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define ib_link_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define ib_link_GetValue()           PORTBbits.RB7
#define ib_link_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define ib_link_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define ib_link_SetPullup()          do { WPUBbits.WPUB7 = 1; } while(0)
#define ib_link_ResetPullup()        do { WPUBbits.WPUB7 = 0; } while(0)
#define ib_link_SetPushPull()        do { ODCONBbits.ODCB7 = 0; } while(0)
#define ib_link_SetOpenDrain()       do { ODCONBbits.ODCB7 = 1; } while(0)
#define ib_link_SetAnalogMode()      do { ANSELBbits.ANSB7 = 1; } while(0)
#define ib_link_SetDigitalMode()     do { ANSELBbits.ANSB7 = 0; } while(0)

// get/set tmp_ana aliases
#define tmp_ana_TRIS                 TRISCbits.TRISC0
#define tmp_ana_LAT                  LATCbits.LATC0
#define tmp_ana_PORT                 PORTCbits.RC0
#define tmp_ana_WPU                  WPUCbits.WPUC0
#define tmp_ana_OD                   ODCONCbits.ODCC0
#define tmp_ana_ANS                  ANSELCbits.ANSC0
#define tmp_ana_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define tmp_ana_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define tmp_ana_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define tmp_ana_GetValue()           PORTCbits.RC0
#define tmp_ana_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define tmp_ana_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define tmp_ana_SetPullup()          do { WPUCbits.WPUC0 = 1; } while(0)
#define tmp_ana_ResetPullup()        do { WPUCbits.WPUC0 = 0; } while(0)
#define tmp_ana_SetPushPull()        do { ODCONCbits.ODCC0 = 0; } while(0)
#define tmp_ana_SetOpenDrain()       do { ODCONCbits.ODCC0 = 1; } while(0)
#define tmp_ana_SetAnalogMode()      do { ANSELCbits.ANSC0 = 1; } while(0)
#define tmp_ana_SetDigitalMode()     do { ANSELCbits.ANSC0 = 0; } while(0)

// get/set pow_ana aliases
#define pow_ana_TRIS                 TRISCbits.TRISC1
#define pow_ana_LAT                  LATCbits.LATC1
#define pow_ana_PORT                 PORTCbits.RC1
#define pow_ana_WPU                  WPUCbits.WPUC1
#define pow_ana_OD                   ODCONCbits.ODCC1
#define pow_ana_ANS                  ANSELCbits.ANSC1
#define pow_ana_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define pow_ana_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define pow_ana_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define pow_ana_GetValue()           PORTCbits.RC1
#define pow_ana_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define pow_ana_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define pow_ana_SetPullup()          do { WPUCbits.WPUC1 = 1; } while(0)
#define pow_ana_ResetPullup()        do { WPUCbits.WPUC1 = 0; } while(0)
#define pow_ana_SetPushPull()        do { ODCONCbits.ODCC1 = 0; } while(0)
#define pow_ana_SetOpenDrain()       do { ODCONCbits.ODCC1 = 1; } while(0)
#define pow_ana_SetAnalogMode()      do { ANSELCbits.ANSC1 = 1; } while(0)
#define pow_ana_SetDigitalMode()     do { ANSELCbits.ANSC1 = 0; } while(0)

// get/set pow_cmd aliases
#define pow_cmd_TRIS                 TRISCbits.TRISC2
#define pow_cmd_LAT                  LATCbits.LATC2
#define pow_cmd_PORT                 PORTCbits.RC2
#define pow_cmd_WPU                  WPUCbits.WPUC2
#define pow_cmd_OD                   ODCONCbits.ODCC2
#define pow_cmd_ANS                  ANSELCbits.ANSC2
#define pow_cmd_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define pow_cmd_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define pow_cmd_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define pow_cmd_GetValue()           PORTCbits.RC2
#define pow_cmd_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define pow_cmd_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define pow_cmd_SetPullup()          do { WPUCbits.WPUC2 = 1; } while(0)
#define pow_cmd_ResetPullup()        do { WPUCbits.WPUC2 = 0; } while(0)
#define pow_cmd_SetPushPull()        do { ODCONCbits.ODCC2 = 0; } while(0)
#define pow_cmd_SetOpenDrain()       do { ODCONCbits.ODCC2 = 1; } while(0)
#define pow_cmd_SetAnalogMode()      do { ANSELCbits.ANSC2 = 1; } while(0)
#define pow_cmd_SetDigitalMode()     do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set cmd_ana aliases
#define cmd_ana_TRIS                 TRISCbits.TRISC3
#define cmd_ana_LAT                  LATCbits.LATC3
#define cmd_ana_PORT                 PORTCbits.RC3
#define cmd_ana_WPU                  WPUCbits.WPUC3
#define cmd_ana_OD                   ODCONCbits.ODCC3
#define cmd_ana_ANS                  ANSELCbits.ANSC3
#define cmd_ana_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define cmd_ana_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define cmd_ana_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define cmd_ana_GetValue()           PORTCbits.RC3
#define cmd_ana_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define cmd_ana_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define cmd_ana_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define cmd_ana_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define cmd_ana_SetPushPull()        do { ODCONCbits.ODCC3 = 0; } while(0)
#define cmd_ana_SetOpenDrain()       do { ODCONCbits.ODCC3 = 1; } while(0)
#define cmd_ana_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define cmd_ana_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set npwm_en aliases
#define npwm_en_TRIS                 TRISCbits.TRISC4
#define npwm_en_LAT                  LATCbits.LATC4
#define npwm_en_PORT                 PORTCbits.RC4
#define npwm_en_WPU                  WPUCbits.WPUC4
#define npwm_en_OD                   ODCONCbits.ODCC4
#define npwm_en_ANS                  ANSELCbits.ANSC4
#define npwm_en_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define npwm_en_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define npwm_en_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define npwm_en_GetValue()           PORTCbits.RC4
#define npwm_en_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define npwm_en_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define npwm_en_SetPullup()          do { WPUCbits.WPUC4 = 1; } while(0)
#define npwm_en_ResetPullup()        do { WPUCbits.WPUC4 = 0; } while(0)
#define npwm_en_SetPushPull()        do { ODCONCbits.ODCC4 = 0; } while(0)
#define npwm_en_SetOpenDrain()       do { ODCONCbits.ODCC4 = 1; } while(0)
#define npwm_en_SetAnalogMode()      do { ANSELCbits.ANSC4 = 1; } while(0)
#define npwm_en_SetDigitalMode()     do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set pwm_ana aliases
#define pwm_ana_TRIS                 TRISCbits.TRISC5
#define pwm_ana_LAT                  LATCbits.LATC5
#define pwm_ana_PORT                 PORTCbits.RC5
#define pwm_ana_WPU                  WPUCbits.WPUC5
#define pwm_ana_OD                   ODCONCbits.ODCC5
#define pwm_ana_ANS                  ANSELCbits.ANSC5
#define pwm_ana_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define pwm_ana_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define pwm_ana_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define pwm_ana_GetValue()           PORTCbits.RC5
#define pwm_ana_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define pwm_ana_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define pwm_ana_SetPullup()          do { WPUCbits.WPUC5 = 1; } while(0)
#define pwm_ana_ResetPullup()        do { WPUCbits.WPUC5 = 0; } while(0)
#define pwm_ana_SetPushPull()        do { ODCONCbits.ODCC5 = 0; } while(0)
#define pwm_ana_SetOpenDrain()       do { ODCONCbits.ODCC5 = 1; } while(0)
#define pwm_ana_SetAnalogMode()      do { ANSELCbits.ANSC5 = 1; } while(0)
#define pwm_ana_SetDigitalMode()     do { ANSELCbits.ANSC5 = 0; } while(0)

// get/set message aliases
#define message_TRIS                 TRISCbits.TRISC6
#define message_LAT                  LATCbits.LATC6
#define message_PORT                 PORTCbits.RC6
#define message_WPU                  WPUCbits.WPUC6
#define message_OD                   ODCONCbits.ODCC6
#define message_ANS                  ANSELCbits.ANSC6
#define message_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define message_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define message_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define message_GetValue()           PORTCbits.RC6
#define message_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define message_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define message_SetPullup()          do { WPUCbits.WPUC6 = 1; } while(0)
#define message_ResetPullup()        do { WPUCbits.WPUC6 = 0; } while(0)
#define message_SetPushPull()        do { ODCONCbits.ODCC6 = 0; } while(0)
#define message_SetOpenDrain()       do { ODCONCbits.ODCC6 = 1; } while(0)
#define message_SetAnalogMode()      do { ANSELCbits.ANSC6 = 1; } while(0)
#define message_SetDigitalMode()     do { ANSELCbits.ANSC6 = 0; } while(0)

// get/set keys_on aliases
#define keys_on_TRIS                 TRISCbits.TRISC7
#define keys_on_LAT                  LATCbits.LATC7
#define keys_on_PORT                 PORTCbits.RC7
#define keys_on_WPU                  WPUCbits.WPUC7
#define keys_on_OD                   ODCONCbits.ODCC7
#define keys_on_ANS                  ANSELCbits.ANSC7
#define keys_on_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define keys_on_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define keys_on_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define keys_on_GetValue()           PORTCbits.RC7
#define keys_on_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define keys_on_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define keys_on_SetPullup()          do { WPUCbits.WPUC7 = 1; } while(0)
#define keys_on_ResetPullup()        do { WPUCbits.WPUC7 = 0; } while(0)
#define keys_on_SetPushPull()        do { ODCONCbits.ODCC7 = 0; } while(0)
#define keys_on_SetOpenDrain()       do { ODCONCbits.ODCC7 = 1; } while(0)
#define keys_on_SetAnalogMode()      do { ANSELCbits.ANSC7 = 1; } while(0)
#define keys_on_SetDigitalMode()     do { ANSELCbits.ANSC7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

#endif // PIN_MANAGER_H
