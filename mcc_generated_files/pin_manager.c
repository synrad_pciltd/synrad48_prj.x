/**
  Generated Pin Manager File

  Company:
    Microchip Technology Inc.

  File Name:
    pin_manager.c

  Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for pin APIs for all pins selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.3
        Device            :  PIC16F18344
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.20 and above
        MPLAB             :  MPLAB X 5.40

    Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "pin_manager.h"

/**
 * Start the board with the following PIN configuration in
 * PIN_MANAGER_Initialize().
 * 
 * PORTA:RA2 as output. shutdown signal
 * PORTB:RB4 as input. keyr_En signal
 * PORTB:RB5 as input. lsec_tm is LSI specific 5 sec startup timer
 * PORTB:RB6 as input. msg_ctrl is unused as input.
 * PORTB:RB7 as input. IB_Link is used as the external fault pin
 * PORTC:RC6 as input. message is never used as input.
 * PORTC:RC7 as input. keys_on is used as indicator if laser key is switched on.
 * PORTC:RC4 as output. npwm_en, is "Not PWM Enabled" (reverse logic)
 * PORTC:RC2 as output. pow_cmd, power available to PWM FETs
 * PORTC:RC3 as analog input. cmd-in value 
 * PORTC:RC1 as analog input. power value supplied to the board
 * PORTC:RC0 as analog input. temperature reported by thermistor
 * PORTC:RC5 as analog input. pwm-out value 
 *  
 * Some ports have multiple purpose defined below. But we only switch their 
 * direction when already needed.
 * 
 * PORTB:RB4 as output. is led1
 * PORTB:RB5 as output. is led2
 * PORTB:RB6 as output. is led3
 * PORTB:RB7 as output. Outbound message that our board is faulting.
 * PORTC:RC6 as output. Message is used to raise Temperature warning event.
 * 
 * 
 */
void PIN_MANAGER_Initialize(void)
{
    /**
    LATx registers
    */
    LATA = 0x00;
    LATB = 0x00;
    LATC = 0x00;

    /**
    TRISx registers
    */
    TRISA = 0x33;
    TRISB = 0xF0;
    TRISC = 0xAB;

    /**
    ANSELx registers
    */
    ANSELC = 0x2B;
    ANSELB = 0x00;
    ANSELA = 0x33;

    /**
    WPUx registers
    */
    WPUB = 0x00;
    WPUA = 0x00;
    WPUC = 0x00;

    /**
    ODx registers
    */
    ODCONA = 0x00;
    ODCONB = 0x00;
    ODCONC = 0x00;

    /**
    SLRCONx registers
    */
    SLRCONA = 0x37;
    SLRCONB = 0xF0;
    SLRCONC = 0xFF;

    /**
    INLVLx registers
    */
    INLVLA = 0x3F;
    INLVLB = 0xF0;
    INLVLC = 0xFF;
}
