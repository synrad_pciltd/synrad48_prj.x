#include "synrad48.h"
#include "pic16f18344_pciltd.h"

#define SHUTDOWN_PIN            2
#define KEY_RESET_EN_PIN        12    
#define LED1_PIN                12
#define LONG_TIMER_SEL_PIN      13
#define LED2_PIN                13
#define MSG_CNTRL_PIN           14
#define LED3_PIN                14
#define IBLINK_PIN              15
#define MESSAGE_PIN             22
#define KEYS_ON_PIN             23
#define NOT_PWM_EN_PIN          20
#define POW_CMD_PIN             18
#define CMD_ANA_PIN             19
#define POW_ANA_PIN             17
#define TEMP_ANA_PIN            16
#define PWM_ANA_PIN             21

/*
 * these signals never change direction:
 * 
 * PORTA:RA2 is output, shutdown signal
 * PORTC:RC7 is input, keys_on signal
 * PORTC:RC4 is output, not_pwm_en signal
 * PORTC:RC2 is output, cmd_in signal
 * PORTC:RC3 is analog input, cmd_ana signal
 * PORTC:RC1 is analog input, pow_ana signal
 * PORTC:RC0 is analog input, temp_ana signal
 * PORTC:RC5 is analog input, pwm_ana signal
 *
 * these signals have dual purpose depending on direction:
 * 
 * PORTB:RB4 is input, key_reset_en signal when input
 * PORTB:RB5 is input, long_timer_select when when input
 * PORTB:RB6 is input, msg_ctrl when input (but it is unused as input)
 * PORTB:RB7 is input, iblink is extfault status of other board when input 
 * PORTC:RC6 is input, message when input, (but message is never used as input)
 */
static const struct portdef_struct pic16f18344_ports[] = {
        { .valid = 1, .port = 1, .bit = 0, .input = 1, .analog = 1, },  /* 0, ra0 */
        { .valid = 1, .port = 1, .bit = 1, .input = 1, .analog = 1, },  /* 1, ra1 */
        { .valid = 1, .port = 1, .bit = 2, .input = 0, .analog = 0, },  /* 2, ra2 */
        { .valid = 0, .port = 1, .bit = 3, .input = 0, .analog = 0, },  /* 3, ra3 */
        { .valid = 1, .port = 1, .bit = 4, .input = 1, .analog = 1, },  /* 4, ra4 */
        { .valid = 1, .port = 1, .bit = 5, .input = 1, .analog = 1, },  /* 5, ra5 */
        { .valid = 0, .port = 1, .bit = 6, .input = 0, .analog = 0, },  /* 6, ra6 */
        { .valid = 0, .port = 1, .bit = 7, .input = 0, .analog = 0, },  /* 7, ra7 */
        { .valid = 0, .port = 2, .bit = 0, .input = 0, .analog = 0, },  /* 8, rb0 */
        { .valid = 0, .port = 2, .bit = 1, .input = 0, .analog = 0, },  /* 9, rb1 */
        { .valid = 0, .port = 2, .bit = 2, .input = 0, .analog = 0, },  /* 10, rb2 */
        { .valid = 0, .port = 2, .bit = 3, .input = 0, .analog = 0, },  /* 11, rb3 */
        { .valid = 1, .port = 2, .bit = 4, .input = 1, .analog = 0, },  /* 12, rb4 */
        { .valid = 1, .port = 2, .bit = 5, .input = 1, .analog = 0, },  /* 13, rb5 */
        { .valid = 1, .port = 2, .bit = 6, .input = 1, .analog = 0, },  /* 14, rb6 */
        { .valid = 1, .port = 2, .bit = 7, .input = 1, .analog = 0, },  /* 15, rb7 */
        { .valid = 1, .port = 3, .bit = 0, .input = 1, .analog = 1, .chs = 0x10},  /* 16, rc0 */
        { .valid = 1, .port = 3, .bit = 1, .input = 1, .analog = 1, .chs = 0x11},  /* 17, rc1 */
        { .valid = 1, .port = 3, .bit = 2, .input = 0, .analog = 0, },  /* 18, rc2 */
        { .valid = 1, .port = 3, .bit = 3, .input = 1, .analog = 1, .chs = 0x13},  /* 19, rc3 */
        { .valid = 1, .port = 3, .bit = 4, .input = 0, .analog = 0, },  /* 20, rc4 */
        { .valid = 1, .port = 3, .bit = 5, .input = 1, .analog = 1, .chs = 0x15},  /* 21, rc5 */
        { .valid = 1, .port = 3, .bit = 6, .input = 0, .analog = 0, },  /* 22, rc6 */
        { .valid = 1, .port = 3, .bit = 7, .input = 1, .analog = 0, },  /* 23, rc7 */
        
        /* end sentinel */
        { 0 },
};

#define LED_MODE        (1 << 0)   /* set when PINs being used for LEDs */
#define FAULT_MODE      (1 << 1)   /* set when board is faulting */
#define TEMP_MODE       (1 << 2)   /* set when using pins as temperature signal */
#define SHUTDOWN_MODE   (1 << 3)   /* set when board encountered error last time */

static struct board_parm_struct {
        struct portdef_struct *ports;
        int flags;
} board_parm = {
        .ports = (struct portdef_struct *)&pic16f18344_ports[0],
        .flags = 0,
};

static void pic16f18344_init_pmd(void)
{
        PMD0 = PMD0_RESET_VAL; // 0x47;
        PMD1 = PMD1_RESET_VAL; // 0xFE;
        PMD2 = PMD2_RESET_VAL; // 0x46;
        PMD3 = PMD3_RESET_VAL; // 0xFF;
        PMD4 = PMD4_RESET_VAL; // 0x22;
        PMD5 = PMD5_RESET_VAL; // 0x1F;
        return;
}

static void pic16f18344_init_adc(void)
{
        ADCON0 = 0x01;

        // ADFM left; ADNREF VSS; ADPREF VDD; ADCS FOSC/4; 
        ADCON1 = 0x90;  // Fosc/8 like the old firmware, adfm is right justified

        // ADACT no_auto_trigger; 
        ADACT = 0x00;

        // ADRESL 0; 
        ADRESL = 0x00;

        // ADRESH 0; 
        ADRESH = 0x00;

        return;
}

static void set_port_dir(struct portdef_struct *port_ptr, const int input)
{
        unsigned char tris_val;
        unsigned char tris_mask;
        
        if (!port_ptr->valid)
                return;
        
        tris_mask = 1 << port_ptr->bit;
        if (port_ptr->port == 1) {
                tris_val = TRISA;
                if (input)
                        TRISA = tris_val | tris_mask;
                else
                        TRISA = tris_val & ~tris_mask;
                return;
        }
        if (port_ptr->port == 2) {
                tris_val = TRISB;
                if (input)
                        TRISB = tris_val | tris_mask;
                else
                        TRISB = tris_val & ~tris_mask;
                return;
        }
        if (port_ptr->port == 3) {
                tris_val = TRISC;
                if (input)
                        TRISC = tris_val | tris_mask;
                else
                        TRISC = tris_val & ~tris_mask;
                return;
        }
        return;
 }
 
static void __set_port_type(struct portdef_struct *port_ptr, const int analog)
{
        unsigned char ansel_val;
        unsigned char ansel_mask;
        
        if (!port_ptr->valid)
                return;
        
        ansel_mask = 1 << port_ptr->bit;
        if (port_ptr->port == 1) {
                ansel_val = ANSELA;
                if (analog)
                        ANSELA = ansel_val | ansel_mask;
                else
                        ANSELA = ansel_val & ~ansel_mask;
                return;
        }
        if (port_ptr->port == 2) {
                ansel_val = ANSELB;
                if (analog)
                        ANSELB = ansel_val | ansel_mask;
                else
                        ANSELB = ansel_val & ~ansel_mask;
                return;
        }
        if (port_ptr->port == 3) {
                ansel_val = ANSELC;
                if (analog)
                        ANSELC = ansel_val | ansel_mask;
                else
                        ANSELC = ansel_val & ~ansel_mask;
                return;
        }
        return;
}

 
static void pic16f18344_init_pin(void)
{
        struct portdef_struct *port_ptr = (struct portdef_struct *)&pic16f18344_ports[0];

        /* clear all port latches */
        LATA = LATB = LATC = 0x00;
        for (; port_ptr->port != 0; port_ptr++) {
                if (!port_ptr->valid)
                        continue;
                set_port_dir(port_ptr, port_ptr->input);
                __set_port_type(port_ptr, port_ptr->analog);
        }
        WPUB = WPUA = WPUC = 0x00;
        ODCONA = ODCONB = ODCONC = 0x00;
        SLRCONA = 0x37;
        SLRCONB = 0xF0;
        SLRCONC = 0xFF;
        INLVLA = 0x00;
        INLVLB = 0x00;
        INLVLC = 0x00;
        return;
}

static void pic16f18344_write_digital_pin(struct portdef_struct *port_ptr, const int val)
{
        unsigned char port_mask;

        if (port_ptr->analog)
                return;

        port_mask = 1 << port_ptr->bit;
        if (port_ptr->port == 1) {
                if (val)
                        PORTA = PORTA | port_mask;
                else
                        PORTA = PORTA & ~port_mask;
                return;
        }
        
        if (port_ptr->port == 2) {
                if (val)
                        PORTB = PORTB | port_mask;
                else
                        PORTB = PORTB & ~port_mask;
                return;
        }
        
        if (port_ptr->port == 3) {
                if (val)
                        PORTC = PORTC | port_mask;
                else
                        PORTC = PORTC & ~port_mask;
                return;
        }
        return;
}

static int pic16f18344_read_digital_pin(struct portdef_struct *port_ptr)
{
        unsigned char port_mask;
        
        if (port_ptr->analog)
                return -1;
        
        port_mask = 1 << port_ptr->bit;
        if (port_ptr->port == 1) {
                return ((PORTA & port_mask) == port_mask);
        }
        
        if (port_ptr->port == 2) {
                return ((PORTB & port_mask) == port_mask);
        }
        
        if (port_ptr->port == 3) {
                return ((PORTC & port_mask) == port_mask);
        }
        return -1;
}

static int pic16f18344_read_analog_pin(struct portdef_struct *port_ptr)
{
#define ACQ_US_DELAY    100
        
        if (!port_ptr->analog || !port_ptr->input)
                return -1;
        
        /* select the channel */
        ADCON0bits.CHS = port_ptr->chs;
        
        /* acquisition time delay */
        __delay_us(ACQ_US_DELAY);
        
        /* start the conversion */
        ADCON0bits.ADGO = 1;
        
        /* wait for the adc to finish */
        while(ADCON0bits.ADGO == 1);
        
        /* return the result now */
        return (int)((ADRESH << 8) | ADRESL);
}

static int pic16f18344_read_power_analog(struct series48_control_board_struct *brd)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        return pic16f18344_read_analog_pin(&ports[POW_ANA_PIN]);
}


static int pic16f18344_key_reset_enabled(struct series48_control_board_struct *brd)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        return pic16f18344_read_digital_pin(&ports[KEY_RESET_EN_PIN]);
}

static int pic16f18344_key_switch_on(struct series48_control_board_struct *brd)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        return pic16f18344_read_digital_pin(&ports[KEYS_ON_PIN]);
}

static void pic16f18344_board_power_en(struct series48_control_board_struct *brd, const int on)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        pic16f18344_write_digital_pin(&ports[POW_CMD_PIN], on);
        return;
}

static int pic16f18344_long_timer_sel_check(struct series48_control_board_struct *brd)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        return pic16f18344_read_digital_pin(&ports[LONG_TIMER_SEL_PIN]);
}

static int pic16f18344_read_temp_analog(struct series48_control_board_struct *brd)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        return pic16f18344_read_analog_pin(&ports[TEMP_ANA_PIN]);
}

static void pic16f18344_pwm_switch(struct series48_control_board_struct *brd,
                                   const int enable)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        pic16f18344_write_digital_pin(&ports[NOT_PWM_EN_PIN], !enable); // reverse logic since N_PWM_en
        return;
}

static void pic16f18344_reset_board(struct series48_control_board_struct *brd)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        struct portdef_struct *port_ptr;

        /* reset the LED pins to default direction again */
        if (parm->flags & LED_MODE) {
                port_ptr = &ports[LED1_PIN];
                pic16f18344_write_digital_pin(port_ptr, 0);
                set_port_dir(port_ptr, port_ptr->input);
                port_ptr = &ports[LED2_PIN];
                pic16f18344_write_digital_pin(port_ptr, 0);
                set_port_dir(port_ptr, port_ptr->input);
                port_ptr = &ports[LED3_PIN];
                pic16f18344_write_digital_pin(port_ptr, 0);
                set_port_dir(port_ptr, port_ptr->input);
                parm->flags &= ~LED_MODE;
        }

        /* reset iblink pin to default direction again */
        if (parm->flags & FAULT_MODE) {
                port_ptr = &ports[IBLINK_PIN];
                pic16f18344_write_digital_pin(port_ptr, 0);
                set_port_dir(port_ptr, port_ptr->input);
                parm->flags &= ~FAULT_MODE;
        }

        /* clear message pin  */
        if (parm->flags & TEMP_MODE) {
                port_ptr = &ports[MESSAGE_PIN];
                pic16f18344_write_digital_pin(port_ptr, 0);
                parm->flags &= ~TEMP_MODE;
        }

        /* clear the shutdown line */
        if (parm->flags & SHUTDOWN_MODE) {
                port_ptr = &ports[SHUTDOWN_PIN];
                pic16f18344_write_digital_pin(port_ptr, 0);
                parm->flags &= ~SHUTDOWN_MODE;
        }

        return;
}

static void pic16f18344_set_led(struct series48_control_board_struct *brd,
                                const struct ledseq_struct *pattern)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;

        if (!(parm->flags & LED_MODE)) 
                return;
        pic16f18344_write_digital_pin(&ports[LED1_PIN], pattern->led1);
        pic16f18344_write_digital_pin(&ports[LED2_PIN], pattern->led2);
        pic16f18344_write_digital_pin(&ports[LED3_PIN], pattern->led3);
        return;
}

static int pic16f18344_ext_fault(struct series48_control_board_struct *brd)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        return pic16f18344_read_digital_pin(&ports[IBLINK_PIN]);
}

static int pic16f18344_read_cmdin(struct series48_control_board_struct *brd)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        return pic16f18344_read_analog_pin(&ports[CMD_ANA_PIN]);
}

static int pic16f18344_read_pwmout(struct series48_control_board_struct *brd)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        return pic16f18344_read_analog_pin(&ports[PWM_ANA_PIN]);
}

static void pic16f18344_shutdown_board(struct series48_control_board_struct *brd)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        struct portdef_struct *port_ptr = &ports[SHUTDOWN_PIN];

        pic16f18344_write_digital_pin(port_ptr, 1);
        parm->flags |= SHUTDOWN_MODE;

        port_ptr = &ports[IBLINK_PIN];
        set_port_dir(port_ptr, 0);
        pic16f18344_write_digital_pin(port_ptr, 1);
        parm->flags |= FAULT_MODE;

        /* also need to enable the output drivers for pins connected to LEDs */
        port_ptr = &ports[LED1_PIN];
        pic16f18344_write_digital_pin(port_ptr, 0);
        set_port_dir(port_ptr, 0);
        port_ptr = &ports[LED2_PIN];
        pic16f18344_write_digital_pin(port_ptr, 0);
        set_port_dir(port_ptr, 0);
        port_ptr = &ports[LED3_PIN];
        pic16f18344_write_digital_pin(port_ptr, 0);
        set_port_dir(port_ptr, 0);
        parm->flags |= LED_MODE;

        return;
}

static void pic16f18344_set_temp_warning(struct series48_control_board_struct *brd,
                                         const int on)
{
        struct board_parm_struct *parm = (struct board_parm_struct *)brd->private;
        struct portdef_struct *ports = parm->ports;
        struct portdef_struct *port_ptr = &ports[MESSAGE_PIN];
        set_port_dir(port_ptr, 0);
        pic16f18344_write_digital_pin(port_ptr, on);
        parm->flags |= TEMP_MODE;
        return;
}

static struct series48_control_board_struct series48 = {
        .read_power_analog = pic16f18344_read_power_analog,
        .key_reset_stat = pic16f18344_key_reset_enabled,
        .key_stat = pic16f18344_key_switch_on,
        .board_power_en = pic16f18344_board_power_en,
        .long_timer_sel_check = pic16f18344_long_timer_sel_check,
        .read_temp_analog = pic16f18344_read_temp_analog,
        .set_temp_warning = pic16f18344_set_temp_warning,
        .read_ext_fault = pic16f18344_ext_fault,
        .read_cmdin = pic16f18344_read_cmdin,
        .read_pwmout = pic16f18344_read_pwmout,
        .pwm_switch = pic16f18344_pwm_switch,
        .reset_board = pic16f18344_reset_board,
        .shutdown_board = pic16f18344_shutdown_board,
        .set_led = pic16f18344_set_led,
        .private = (struct board_parm_struct *)&board_parm,
} ;

void main(void)
{
        pic16f18344_init_pmd();
        pic16f18344_init_pin();
        pic16f18344_init_adc();
        register_synrad48_control_board(&series48);
        return;
}
