#ifndef PIC16F18344_PCILTD_H
#define	PIC16F18344_PCILTD_H

#ifdef	__cplusplus
extern "C" {
#endif

/* PMD0 definitions */
#define PMD0_IOCDM      (1 << 0)
#define PMD0_CLKRMD     (1 << 1)
#define PMD0_NVMMD      (1 << 2)
#define PMD0_FVRMD      (1 << 6)
#define PMD0_SYSCMD     (1 << 7)
#define PMD0_RESET_VAL  (PMD0_FVRMD | PMD0_NVMMD | PMD0_CLKRMD | PMD0_IOCDM)

/* PMD1 definitions */
#define PMD1_TMR0MD     (1 << 0)
#define PMD1_TMR1MD     (1 << 1)
#define PMD1_TMR2MD     (1 << 2)
#define PMD1_TMR3MD     (1 << 3)
#define PMD1_TMR4MD     (1 << 4)
#define PMD1_TMR5MD     (1 << 5)
#define PMD1_TMR6MD     (1 << 6)
#define PMD1_NCOMD      (1 << 7)
#define PMD1_RESET_VAL  (  PMD1_NCOMD | PMD1_TMR6MD | PMD1_TMR5MD| PMD1_TMR4MD \
                         | PMD1_TMR3MD | PMD1_TMR2MD | PMD1_TMR1MD)

/* PMD2 definitions */
#define PMD2_CMP1MD     (1 << 1)
#define PMD2_CMP2MD     (1 << 2)
#define PMD2_ADCMD      (1 << 5)
#define PMD2_DACMD      (1 << 6)
#define PMD2_RESET_VAL  (PMD2_DACMD | PMD2_CMP2MD | PMD2_CMP1MD)        

/* PMD3 definition */
#define PMD3_CCP1MD     (1 << 0)
#define PMD3_CCP2MD     (1 << 1)
#define PMD3_CCP3MD     (1 << 2)
#define PMD3_CCP4MD     (1 << 3)
#define PMD3_PWM5MD     (1 << 4)
#define PMD3_PWM6MD     (1 << 5)
#define PMD3_CWG1MD     (1 << 6)
#define PMD3_CWG2MD     (1 << 7)
#define PMD3_RESET_VAL  (  PMD3_CWG2MD | PMD3_CWG1MD | PMD3_PWM6MD \
                         | PMD3_PWM5MD | PMD3_CCP4MD | PMD3_CCP3MD \
                         | PMD3_CCP2MD | PMD3_CCP1MD)

/* PMD4 definition */
#define PMD4_MSSP1MD    (1 << 1)
#define PMD4_UART1MD    (1 << 5)
#define PMD4_RESET_VAL  (PMD4_UART1MD | PMD4_MSSP1MD)

/* PMD5 definition */
#define PMD5_DSMMD      (1 << 0)
#define PMD5_CLC1MD     (1 << 1)
#define PMD5_CLC2MD     (1 << 2)
#define PMD5_CLC3MD     (1 << 3)
#define PMD5_CLC4MD     (1 << 4)
#define PMD5_RESET_VAL  (  PMD5_CLC4MD | PMD5_CLC3MD | PMD5_CLC2MD \
                         | PMD5_CLC1MD | PMD5_DSMMD)

#define OSCCON1_RESET_VALUE     0x70

struct portdef_struct {
        unsigned valid  : 1;
        unsigned input  : 1; /* init value of the pin */
        unsigned analog : 1;
        unsigned port   : 2;
        unsigned bit    : 3;
        unsigned chs    : 6;
};

#ifdef	__cplusplus
}
#endif

#endif	/* PIC16F18344_PCILTD_H */
